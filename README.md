# Self-crafted Hello World Docker Image

Fork this repository to host your own Hello World Docker image on the GitLab Container Registry.

The registry of this repository is available at `registry.gitlab.unige.ch/your-namespace/helloworld`.

## Login

First, you need to login to the registry using your GitLab credentials:

```shell
docker login registry.gitlab.unige.ch
```

## Publish

To build and push your own image, you can use the following commands:

```shell
docker build -t registry.gitlab.unige.ch/your-namespace/helloworld .
docker push registry.gitlab.unige.ch/your-namespace/helloworld
```
